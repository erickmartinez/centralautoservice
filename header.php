<!DOCTYPE HTML>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<meta charset="UTF-8">
	</head>

	<body>
		<div id="wrapper">
		<header>
			<a href="./index.php" id="logo-header"><img src="./img/logo.png"></a>
			<h3>Compra y consignacion de automóviles</h3>
			<nav id="menu">
				<ul>
					<li><a href="#">Inicio</a></li>
					<li><a href="#">Galería</a></li>
					<li><a href="#">Contáctanos</a></li>
					<li><a href="#">Ubicación</a></li>
				</ul>
			</nav>
		</header>